% Ryan Leonard -- Winter 2016
\documentclass{journal}
% Used to format our math sections
%\usepackage[margin=1in]{geometry}
\usepackage{comment}
\usepackage{graphicx}
\usepackage{color}
\usepackage{lscape}
\usepackage{listings}
\lstset{language=C++,
                basicstyle=\ttfamily,
                keywordstyle=\color{blue}\ttfamily,
                stringstyle=\color{red}\ttfamily,
                commentstyle=\color{green}\ttfamily,
                morecomment=[l][\color{magenta}]{\#},
                numbers=left
}

\author{Ryan Leonard}
\title{Perceptron Prefetch Engine}
\subtitle{A Machine Learning Approach to Data Cache Prefetching\\CIS 553 Final Report}

\begin{document}
\maketitle


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%	Outline
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{comment}
% what is the problem and data sets addressed by the project,
1. Background \& Domain Knowledge -- CPU Mem, Memory Hierarchy based on locality, Load and Store instructions are bread and butter, cache misses are costly and we want to minimize them. 
2. Simulation Framework -- 
% what are the data mining algorithms used,
3. Perceptron Prefetch Engine -- Realtime data processing, real-time perceptron training, real-time perceptron predictions.
% what are the knowledge or pattern discovered from the project,
4. Results -- Our very naive Perceptron based on processed stride history does \textbf{not} perform well.
% what are your evaluations and ideas for extensions and improvements.
5. Future Work -- (1) Change the attribute set using different digesting techniques or more attributes (2) play with window size (3) Only fire perceptron on cache miss (4) Review total prefetches of all Sequential prefetch methodologies (5) vary cache parameters -- maybe works better with smaller block size/smaller cache/less associative (6) multilayered perceptron (7) determine if hardware simulations is possible.
\end{comment}
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%	Introduction
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction} 
\label{s:intro}

% The issue -- memory hierarchies
Using a memory hierarchy is a commonly accepted solution for bridging the gap between fast CPUs and relatively slow memory units. As computer architects, to maximize the benefits of a memory hierarchy, we do our best to leverage memory access patterns of applications to keep only the essential data in the fastest portions of our memory hierarchy. Typical schemes used to leverage memory hierarchies are based on simple heuristics and basic principles; i.e. the \textit{Principle of Locality}.

% Prefetching as a fix
One method to attempt to increase the performance and efficiency of our memory hierarchy is to \textit{prefetch} data from slow memory into our faster caches just before it would otherwise be loaded into our CPU. The goal of prefetching is to minimize our \textit{cache miss rate} and consequently minimize our \textit{memory access penalty}.

% Our addition
We reviewed current prefetching prediction heuristics and propose a novel method for predicting future memory accesses. Namely, we propose a new system component: a Perceptron Prefetch Engine. Our engine uses a small sliding window of recent memory accesses to update a \textit{perceptron} then uses that perceptron to predect whether or not to perform a prefetch. Benefits of using a prefetch engine include (1) this engine has potential to scale better than other prefetch heuristics, and (2) we believe that the concrete concept of time inherit in our sliding window provides additional signal inherit to our applications' memory access patterns. Current prefetching prediction heuristics we have reviewed indirectly leverage such signal; our direct approach is proposed to have more prediction power.

% Simul
To evaluate our engine against other schemes, we have built a cache software simulation which runs on top of Intel Pin in real-time while an observed application performs memory accesses. We use this simulation framework to compare several different cache configurations using a subset of the SPEC CPU2000 applications as our workload.

% Results
In this investigation, we have shown that our very naive Perceptron Prefetch Engine implementation is not generally more effective than other prefetching techniques. Our current engine outperforms a basic physical cache over most applications, but is outperformed by basic prefetching technique on most applications. This work has many contributions in its current state, but we hope future contributions will show more efficacy of our novel Perceptron Prefetch Engine.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%	Background
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Background \& Domain Knowledge} 
\label{s:review}



\begin{figure}
\centering
\includegraphics[scale=0.5]{figures/cpu_mem_gap}
\caption{There is a large gap between latency in processor technologies and main memory technologies. As these technologies progress, processor speeds are continuing to increase at a much higher exponential rate than memory speeds. \cite{cpu_mem_gap}}
\label{f:cpu_mem_gap}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=0.7]{figures/cpu_mem_hierarchy}
\caption{A typical CPU memory hierarchy in which the closer we are to the CPU, the less space is available and the more intelligently we have to fill our cache space. Typical access times indicated in the graph. \cite{comparch}}
\label{f:cpu_mem_hierarchy}
\end{figure}

% Memory Hierarchy
Memory is slow, CPUs are fast. \cite{comparch} This results in our processing units often waiting on our memory units and thus decreases the overall performance of any modern computer using a load-store architecture. This problem is only compounding as time progresses and technologies improve. We are gaining processing power at a rate kin to Moore's law dictated in the 1970's, indicating that every 2 years we see a 2$\times$ increase in processing speed and throughput. While our processors continue to increase in capabilities, memory speeds have been increasing at a rate of no more than doubling every 4 years. Until we have a major breakthrough in memory technologies or a major paradigm shift away from load-store architectures, this gap is the biggest hinderance to computing performance. \cite{comparch}

One noteworthy behavior of the vast majority of computing applications is that in some time of an application's execution, we will repeatedly access portions of memory that are close to one another: this is \textit{the principle of temporal and spacial locality}. \cite{comparch} We leverage these principles by using a \textit{memory hierarchy} system in which multiple levels of progressively faster and faster memory are used to store recently accessed data. This allows our processing units to run nearly at full capacity without blocking for slow memory accesses, significantly reducing our \textit{memory access penalty}. See figure \ref{f:cpu_mem_hierarchy}.

% Load and Store instructions are bread and butter, cache misses are costly and we want to minimize them. 
In our load-store computer architectures, our applications and their inputs are stored in memory as an \textit{instruction segment} and \textit{data segment}. In order to execute an application, several \textit{instruction loads} and \textit{data loads/stores} must be performed. Instruction Load: instructions are loaded from an applications' instruction segment into our processing unit then executed. Data Load/Store: several instructions rather than processing data will actually be retrieving and storing data from/to an applications' data segment. We focus on the latter case of load/store instructions as opposed to implicit instruction loads.

Data Load/Store instructions account for some one-third of all instructions. \cite{comparch}
When these instructions execute, if the data is in L1 cache, then we are able to almost immidiately continue execution (cache hit).
If the data is in main memory, then our processing unit must wait for approximately 170 cycles before it can continue execution (cache miss). Notice that in modern systems there are several levels of caches to help minimize the penalty of an L1 cache miss. Minimizing L1 cache misses will result in overall better system performance.

\subsection{Prefetching}
\label{ss:prefetching}
Rather than waiting for a cache miss to occur, an alternative would be to prefetch that data prior to its ever being needed. In an optimal scenario, a prefetching mechanism will be able to anticipate every load instruction that our application will generate. Additionally, the mechanism will anticipate the load with enough time to perform a prefetch that completes one cycle before when the primary processing unit performs a load of that prefetched data. This is indicated in figure \ref{f:prefetch_optimal}.

\begin{figure}[h]
\centering
\includegraphics[scale=0.4]{figures/prefetch_optimal}
\caption{Execution diagram assuming a) no prefetching, b) optimal prefetching, and c) degraded prefetching. \cite{pfsurvey}}
\label{f:prefetch_optimal}
\end{figure}

\subsubsection{One block lookahead}

One Block Lookahead (OBL) is an example of a \textit{sequential prefetching technique}. Sequential prefetching techniques are characterized by that prefetches will always occur on the block based on the current address. I.e. when address $B$ has been fetched and we have determined that prefetching should occur, we will fetch address $B+1$ in a typical OBL prefetching scheme. Sequential prefetching techniques have two parameters: (1) the number of blocks to be prefetched, e.g. two block prefetching, or four block prefetching and (2) some function to determine whether or not to prefetch.

One of the most basic OBL schemes known as \textit{OBL prefetch on miss}. This scheme prefetches one sequential block whenever a cache miss occurs.

Another common OBL scheme is referred to as \textit{OBL tagged prefetching}. This schemes initial principle is the same as the prefetch on miss OBL scheme. The difference is that whenever an address is prefetched, that cacheline is ``prefetch tagged.'' Furthermore, whenever a cache hit occurs, if the hit cache line was tagged, then another prefetch occurs. We notice that this scheme has storage overhead in that every cache line must retain a \textit{prefetch tag bit}. This storage overhead scales linearly with the size of the cache.

% TODO: look at memory access patterns AND how they might be influenced by INSTRUCTION HISTORY as opposed to just memory access history!
% Principles of locality indicate signal --
%There is a signal within our recent history of memory accesses that can predict our future memory accesses. This is indicated by the concept of a \textit{memory access pattern} and notably the principles of temporal and spacial locality. This signal alone may be sufficient to perform good prefetching using machine learning algorithms.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%	Simulation
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Simulation Framework}
\label{s:simulation} 

\begin{figure}[t]
\centering
\begin{lstlisting}
void replacement(const UINT32 &index, const UINT32 &tag) {
	UINT32 i = rpChooseLine(index, tag);	
	cacheTags[index][i] = tag;
}

bool lookup(const UINT32 &index, const UINT32 &tag) {
  for(UINT32 i = 0; i < associativity; i++)
    if (cacheTags[index][i] == tag) 
      return true;     
  return false;
}

UINT8 cacheAccess(const UINT32 &virtualAddr) {
  // Index Lookup
  UINT32 index = getIndex(virtualAddr);

  // Tag Compare
  UINT32 tag = getTag(virtualAddr);
  if (lookup(index, tag))
    return 1;

  // Upon cache miss, now we have to do some cache replacement
  replacement(index, tag);
  return 0;
}
\end{lstlisting}
\caption{Fundamental cacheAccess psuedo code. The logic for choosing which cache block to replace is omitted and this behaviour is stubbed with the rpChooseLine function.}
\label{f:cacheAccess}
\end{figure}


% Where are we getting our data from -- SPEC
We will use the SPEC CPU2000 benchmark suite as our applications for testing. This suite is a collection of applications that represent real-world workloads and applications \cite{spec2000}.

% What is Intel PIN?
We will perform our simulation using Intel Pin. Pin is a dynamic analysis application that allows instrumentation of programs. Pin allows a Pintool to insert arbitrary code known as an \textit{analysis routine} (written in C or C++) throughout an application during execution. Pin provides an API that allows hooks to be placed on individual x86 instructions and provides the ability to investigate the context at the moment of that instructions' execution. \cite{intelpin}

% What is the basics of our simulation structure?
CacheModel is our C++ abstract class which implements many helpers for performing cache maintenance and has a virtual cacheAccess method. The cache is modeled such that only meta data is stored, i.e. the actual data blocks are never recorded in our cache simulation, only the addresses of those data blocks. Our cacheAccess method is implemented by any CacheModel implementation, e.g. BasicCacheModel, OblCacheModel, and OblTaggedCacheModel are each implementations of CacheModel which have their own varying implementations of the cacheAccess method. Abstractly, the cacheAccess method is simply a search function that checks if an address exist in our CacheModel. Our BasicCacheModel cacheAccess implementation is represented in figure \ref{f:cacheAccess}.

% Our simulation
To perform our simulation, we will create a Pintool that hooks each load and store instruction and inserts our analysis routine. This analysis routine will beckon our CacheModel to perform a \textit{cacheAccess}. From an abstract perspective, we think of our simulation as performing the event loop indicated in figure \ref{f:simulation}.

\begin{figure}[h]
\centering
\begin{lstlisting}
CacheModel model = new BasicCacheModel();
while (address != null) {
  address = pin.getNextLoadOrStoreAddress();
  model.cacheAccess(address);
}
model.dumpStatistics();
\end{lstlisting}
\caption{Basic CacheModel simulation psuedo code.}
\label{f:simulation}
\end{figure}
 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%		Prefetch Engine
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Prefetch Engine}
\label{ss:pfe}

The prefetch engine will complete three operations in real-time as the simulation executes. It must perform (1) data processing, (2) model updating, and (3) prediction. These three operations seem that they might have exorbitant cost, but a study of the engine's critical path reveals that this operation can be performed with very low latency, possibly even within one processing clock cycle.

Keeping our code efficient is paramount to keep in line with the goal of eventually performing all of these operations using a hardwired controller. Fortunately, there is a secondary motivation for keeping our code efficient: the execution of each SPEC application on top of our simulation takes a non-negligible amount of time. Running the \textit{basic} cache simulation over all of our applications takes nearly 10 minutes. Keeping our software simulation of our perceptron engine efficient will allow more exhaustive performance analysis.

\subsection{Real-Time Data Processing}

We must process our raw data into attributes that can be handled by our perceptron. Each raw datum is discretized into a binary value of either 1 or -1.

In our current implementation, we have two inputs ``stride'' and ``hit.'' Our raw stride data consists of the stride from the previous address to the current address, i.e. $stride = currentAddress - previousAddress$. Our hit data is a boolean value indicating whether cacheAccess of the currentAddress was a cache hit or a cache miss.

Discretization of our stride is caculated by checking the conditional expression $stride == 1$, if true 1, if false -1. Our hit is already discrete, but is transformed by checking the conditional $hit == true$, if true 1, if false -1. Each of our discretized stride and hit are inserted as $inputs_{t}$.

An example of this logic is seen at line 41 in figure \ref{f:p_cacheAccess}.

\subsection{Real-Time Model Updating}

Model updating is performed using $inputs_{t-k}$ and a psuedo class label. Our class label is calculated based on the logic that a prefetch is necessarily correct if the an address prefetched at time $t-k$ was accessed in time $(t-k+1, t)$. A small value of $k$ will result in our perceptron being more ``up to date'' during our prediction step, while a large value of $k$ will result in a more accurate class label. 

A smaller value of $k$ will be prone to training our model to avoid prefetching. Being that cache misses are very sparse in the regular case i.e. very few prefetches are required, a small value of $k$ seems that it might perform well. In our current implementation, we are using a fixed value $k=1$.

An example of this logic is seen in the updateModel method in figure \ref{f:p_cacheAccess}.

\subsection{Real-Time Prediction}

With an up to date perceptron model and a new datum inserted into our inputs, we will run $inputs_{t}$ through our perceptron to calculate whether or not to perform a prefetch of address $currentAddress + 1$. The output of our perceptron is on of the values 1 or -1. We convert this back to our simulation using the logic $output > 0$ implies prefetch, otherwise do not perform prefetch.

An example of this logic is seen in the predictPrefetch method in figure \ref{f:p_cacheAccess}.

\subsection{Critical Path}
Our prefetch engine's critical path of execution is can be assessed to be performed three steps. $t$ represents the current time in the below description:

\begin{enumerate}
\item Perceptron input value(s) are processed and inserted into our history buffer(s)
\item Perform $update_{t}$ over our perceptron weights and bias based on $activation_{t-1}$. $update_t$ is an embarrassingly parallel process and so is executed in one step.
\item To make our prediction, we calculate $activation_{t}$ (which is stored through time $t+1$ for use in in $update_{t+1}$). $activation_t$ is an embarrassingly parallel process and so is executed in one step.
\end{enumerate}

This very short critical path indicates that a hardware simulation will hopefully prove our prefetch engine to be a plausible option. In the case that all three operations can not be completed in one processing unit's clock cycle, further parallelization can be achieved by pipelining the execution of our engine.


\begin{figure}[h]
\centering
\begin{lstlisting}
INT32 calculateActivation() {
  INT32 ret = 0;
  for (UINT64 i = 0; i < MAX_HIST; i++)
    ret += strideHist[i] * strideWeights[i];
  return ret + perceptronBias;
}

void updateModel(UINT32 index) {
  INT32 activation = calculateActivation();
  INT8 actual = (((index - previousIndex) == 1) ? 1 : -1);
  if (actual*activation <= 0) {
    // Update the bias
    perceptronBias += actual;
    // Update the weights of the perceptron
    for (UINT64 i = 0; i < MAX_HIST; i++)
      strideWeights[i] += actual*strideHist[i];
  }
}
        
bool predictPrefetch() { return (calculateActivation() > 0);}

UINT8 cacheAccess(const UINT32 &virtualAddr) {
  UINT8 ret = 0;
  time = (time + 1) % MAX_HIST;

  // Index Lookup
  UINT32 index = getIndex(virtualAddr);
  // Tag Compare
  UINT32 tag = getTag(virtualAddr);
  if (lookup(index, tag)) {
    ret = 1;
  } else {
    // Upon cache miss, now we have to do some cache replacement
    replacement(index, tag);
  }
    
  // Update Perceptron Model (weights and bias adjustment)
  perceptron = updateModel(perceptron, index);
  // Add current instruction to our sliding window
  strideHist[time] = ((index - previousIndex) == 1) ? 1 : -1;  
  previousIndex = index;
  // Make our prediction about the next instruction
  if (predictPrefetch())
    prefetchAddress(index+1);
  return ret;
}
\end{lstlisting}
\caption{Example perceptron cacheAccess psuedo code. Top three functions are helpers to calculate the perceptron update and prediction. strideWeights and strideHist are integer arrays. MAX\_HIST, perceptronBias, and time are integers.}
\label{f:p_cacheAccess}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%		Results
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results}
\label{s:results}

Given a very robust cache configuration which has a very low miss rate even when running a basic CacheModel (Physical Cache), our Perceptron Prefetch Engine Cache (OBL Perceptron Physical Cache) performs better than the basic cache on most applications. However, our engine driven cache is regularly outperformed on most applications by the more naive prefetching techniques proposed in section \ref{ss:prefetching}. Results are portrayed in figure \ref{f:results}. The CacheModel configuration shared among these different caches is indicated in the table below:

\begin{center}
\begin{tabular}{|l|l|}
\hline
Configuration Variable & Value \\\hline
Cache Size & 64 KB \\
Associativity & 4-way \\
Block Size & 32 B \\\hline
\end{tabular}
\end{center}

In addition to these results, we have also simulated 35 cache configurations using the SPEC CPU2000 benchmark suite running the Basic, OBL, and OBL Tagged models. This data was collected over the course of some 24 cpu execution hours and will prove as an effective baseline/ground truth for many future investigations. Further, this massive data collection could alone be used to perform data mining for finding interesting rules about cache configurations as they relate to the SPEC CPU2000 workload. As an example, see figure \ref{f:datamining} which indicates that with a fixed block size and cache size, increasing the associativity will not always increase the performance of the cache

\begin{landscape}
\begin{figure}
\centering
\includegraphics[scale=0.85]{figures/results}
\caption{Average miss rate of 4 cache models using a very robust cache configuration: Cache Size 64KB, Associativity 4-way, Block Size 32B}
\label{f:results}
\end{figure}
\end{landscape}

\begin{landscape}
\begin{figure}
\centering
\includegraphics[scale=0.85]{figures/results_ass}
\caption{Average miss rate of 3 cache models as associativity varies with configuration: Cache Size 32KB, Block Size Average(8B, 16B, 32B, 64B)}
\label{f:datamining}
\end{figure}
\end{landscape}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%	Conclusions
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions \& Future Direction} 
\label{s:conclusions}

In this work we performed a preliminary simulation and investigation of a Perceptron Prefetch Engine. We have shown that it is not an effective option in its current state when used on top of a well performing cache. This work has laid out a framework for 

We hope future works will show more efficacy of our novel Perceptron Prefetch Engine through the following investigations. First, (1) we will attempt to modify our engines inputs by modifying the basic attribute set. Further, (2) we will attempt to find what history input size results in an effective engine scheme. Additionally, (3) we may attempt to change the times that prefetches can be performed, e.g. perhaps kin to other mechanisms, our engine should only attempt a prefetch if the current instruction results in a cache miss. We note also that (4) more various cache configurations should be compared. Next, (5) we can attempt to move into a more complex perceptron model via the usage of multiple perceptrons and even multilayered Nueral Networks. Such a modification to the engine may result in our model having a better representation of different history sequences as opposed to just looking singularly at the current history. Finally, (6) we must investigate hardware simulations of our Perceptron Prefetch Engines. Although the overhead of adding our engine to our hardware should scale better than other proposed schemes, we need to know what the basic overhead is for implementing our Perceptron Prefetch Engine.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%	Refrences
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliography{citations}{}
\bibliographystyle{plain}

\end{document}
