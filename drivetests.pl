#!/usr/bin/perl
use File::Spec;

# --- System Specifications
# 1GB of memory
# 4KB page size
$system_config = "-m 30 -p 12";


@test = ("test", "testing");

# --- Cache Specifications
# Capacity	2K, 8K, 16K, 32K, 64K, 128K
# Block Size	4B, 16B, 32B, 64B
# Associativity 1, 2, 4, 8
## OMMITTING 2KB cache size for brevity!
## OMMITTING 4B block for brevity!
## Testing Associativity only over 32B block for brevity!
@cache_configs = (
    # 2KB configurations
    #"-r 9 -b 2  -a 1",	# Direct Mapped -- varying block size
    #"-r 8 -b 3  -a 1", 
    #"-r 7 -b 4  -a 1", 
    #"-r 6 -b 5  -a 1", 
    #"-r 5 -b 6  -a 1", 
    #"-r 4 -b 6  -a 2",	# 64 byte blocks
    #"-r 3 -b 6  -a 4", 
    #"-r 2 -b 6  -a 8", 
    #"-r 5 -b 5  -a 2",	# 32 byte blocks
    #"-r 4 -b 5  -a 4", 
    #"-r 3 -b 5  -a 8", 
    #"-r 6 -b 4  -a 2",	# 16 byte blocks
    #"-r 5 -b 4  -a 4", 
    #"-r 4 -b 4  -a 8", 
    # 8KB configurations
    #"-r 11 -b 2  -a 1",	# Direct Mapped -- varying block size
    "-r 10 -b 3  -a 1", 
    "-r 9  -b 4  -a 1", 
    "-r 8  -b 5  -a 1", 
    "-r 7  -b 6  -a 1", 
    #"-r 6  -b 6  -a 2",	# 64 byte blocks
    #"-r 5  -b 6  -a 4", 
    #"-r 4  -b 6  -a 8", 
    "-r 7  -b 5  -a 2",	# 32 byte blocks
    "-r 6  -b 5  -a 4", 
    "-r 5  -b 5  -a 8", 
    #"-r 8  -b 4  -a 2",	# 16 byte blocks
    #"-r 7  -b 4  -a 4", 
    #"-r 6  -b 4  -a 8", 
    # 16KB configurations 
    "-r 11 -b 3  -a 1", # Direct Mapped -- varying block size
    "-r 10 -b 4  -a 1", 
    "-r 9  -b 5  -a 1", 
    "-r 8  -b 6  -a 1", 
    #"-r 7  -b 6  -a 2",	# 64 byte blocks
    #"-r 6  -b 6  -a 4", 
    #"-r 5  -b 6  -a 8", 
    "-r 8  -b 5  -a 2",	# 32 byte blocks
    "-r 7  -b 5  -a 4", 
    "-r 6  -b 5  -a 8", 
    #"-r 9  -b 4  -a 2",	# 16 byte blocks
    #"-r 8  -b 4  -a 4", 
    #"-r 7  -b 4  -a 8", 
    # 32KB configurations
    "-r 12 -b 3  -a 1", # Direct Mapped -- varying block size
    "-r 11 -b 4  -a 1", 
    "-r 10 -b 5  -a 1", 
    "-r 9  -b 6  -a 1", 
    #"-r 8  -b 6  -a 2",	# 64 byte blocks
    #"-r 7  -b 6  -a 4", 
    #"-r 6  -b 6  -a 8", 
    "-r 9  -b 5  -a 2",	# 32 byte blocks
    "-r 8  -b 5  -a 4", 
    "-r 7  -b 5  -a 8", 
    #"-r 10 -b 4  -a 2",	# 16 byte blocks
    #"-r 9  -b 4  -a 4", 
    #"-r 8  -b 4  -a 8", 
    # 64KB configurations
    "-r 13 -b 3  -a 1", # Direct Mapped -- varying block size
    "-r 12 -b 4  -a 1", 
    "-r 11 -b 5  -a 1", 
    "-r 10 -b 6  -a 1", 
    #"-r 9  -b 7  -a 1", # <-- extra large block testing 
    #"-r 8  -b 8  -a 1", # <-- 
    ##"-r 9  -b 6  -a 2",	# 64 byte blocks
    #"-r 8  -b 6  -a 4", 
    #"-r 7  -b 6  -a 8", 
    "-r 10 -b 5  -a 2",	# 32 byte blocks
    "-r 9  -b 5  -a 4", 
    "-r 8  -b 5  -a 8", 
    #"-r 11 -b 4  -a 2",	# 16 byte blocks
    #"-r 10 -b 4  -a 4", 
    #"-r 9  -b 4  -a 8", 
    # 128KB configurations
    "-r 14 -b 3  -a 1", # Direct Mapped -- varying block size
    "-r 13 -b 4  -a 1", 
    "-r 12 -b 5  -a 1", 
    "-r 11 -b 6  -a 1", 
    #"-r 10 -b 7  -a 1", # <-- extra large block testing 
    #"-r 9  -b 8  -a 1", # <-- 
    ##"-r 10 -b 6  -a 2",	# 64 byte blocks
    #"-r 9  -b 6  -a 4", 
    #"-r 8  -b 6  -a 8", 
    "-r 11 -b 5  -a 2",	# 32 byte blocks
    "-r 10 -b 5  -a 4", 
    "-r 9  -b 5  -a 8", 
    #"-r 12 -b 4  -a 2",	# 16 byte blocks
    #"-r 11 -b 4  -a 4", 
    #"-r 10 -b 4  -a 8"
    );

# --- pintool name
$pintool = "/home/leonard/leonard/l2cacheSim/caches";

chomp($pintool);
$localdir = File::Spec->rel2abs( "./" ) ;

#create directory for test produced files
$tempdir = $localdir . "/_temp/";
mkdir($tempdir, 0755);

#create uniquified result directory here
$dirname = localtime();
chomp($dirname);
$dirname =~ s/ /_/g;
$resultdir = $localdir . "/results_" . $dirname . "/";
mkdir($resultdir, 0755);

# Test names basically, an array of arrays. 
# First arg is test name, 
# Second is the directory where the test needs to be executed, mostly ./ works
# Third is the test executable.  
# the remainder are the test args, as would appear in the c argv array
@testcases = (  
        [
        "BZIP 1",
        "./", 
        "/home/caeslab/general-items/PIN/CINT2000/256.bzip2/exe/bzip2_base.x86_linux",
        "/home/caeslab/general-items/PIN/CINT2000/256.bzip2/data/test/input/input.random", 
        "2"
        ],
        [
        "CRAFTY 1",
        "./_temp",
        "/home/caeslab/general-items/PIN/CINT2000/186.crafty/exe/crafty_base.x86_linux",
        "<",
        "/home/caeslab/general-items/PIN/CINT2000/186.crafty/data/test/input/crafty.in"
        ],
        [
        "GAP 1",
        "./",
        "/home/caeslab/general-items/PIN/CINT2000/254.gap/exe/gap_base.x86_linux",  
        "-l",
        "/home/caeslab/general-items/PIN/CINT2000/254.gap/data/all/input",
        "-q",
        "-m",
        "64M",
        "<",
        "/home/caeslab/general-items/PIN/CINT2000/254.gap/data/test/input/test.in"
        ],
        [
        "GCC 1",
        "./",
        "/home/caeslab/general-items/PIN/CINT2000/176.gcc/exe/cc1_base.x86_linux",
        "/home/caeslab/general-items/PIN/CINT2000/176.gcc/data/test/input/cccp.i",
        "-o",
        "${tempdir}/foo.o"
        ],
        [ 
        "GZIP 1",
        "./",
        "/home/caeslab/general-items/PIN/CINT2000/164.gzip/exe/gzip_base.x86_linux",
        "/home/caeslab/general-items/PIN/CINT2000/164.gzip/data/test/input/input.compressed", 
        "2"
        ],
        [
        "PARSER 1",
        "/home/caeslab/general-items/PIN/CINT2000/197.parser/data/all/input/",
        "/home/caeslab/general-items/PIN/CINT2000/197.parser/exe/parser_base.x86_linux",
        "/home/caeslab/general-items/PIN/CINT2000/197.parser/data/all/input/2.1.dict",
        "-batch",
        "<", 
        "/home/caeslab/general-items/PIN/CINT2000/197.parser/data/test/input/test.in"
        ],
        #[
        #"SWIM 1",
        #"./_temp",
        #"/home/caeslab/general-items/PIN/CFP2000/171.swim/exe/swim_base.none",
        #"<",
        #"/home/caeslab/general-items/PIN/CFP2000/171.swim/data/test/input/swim.in"
        #],
        #[
        # "MGRID 1",
        # "./_temp", 
        # "/home/caeslab/general-items/PIN/CFP2000/172.mgrid/exe/mgrid_base.none",
        # "<",
        # "/home/caeslab/general-items/PIN/CFP2000/172.mgrid/data/test/input/mgrid.in"
        #],
        #[
        #"APPLU 1",
        #"./_temp",
        #"/home/caeslab/general-items/PIN/CFP2000/173.applu/exe/applu_base.none",
        #"<",
        #"/home/caeslab/general-items/PIN/CFP2000/173.applu/data/test/input/applu.in"
        #],
        [
        "MESA 1",
        "./_temp",
        "/home/caeslab/general-items/PIN/CFP2000/177.mesa/exe/mesa_base.none",
        "-frames",
        "10",
        "-meshfile",
        "/home/caeslab/general-items/PIN/CFP2000/177.mesa/data/test/input/mesa.in"
        ],
        [
        "ART 1",
        "./_temp",
        "/home/caeslab/general-items/PIN/CFP2000/179.art/exe/art_base.none",
        "-scanfile",
        "/home/caeslab/general-items/PIN/CFP2000/179.art/data/test/input/c756hel.in",
        "-trainfile1",
        "/home/caeslab/general-items/PIN/CFP2000/179.art/data/test/input/a10.img",
        "-stride",
        "2",
        "-startx",
        "134",
        "-starty",
        "220",
        "-endx",
        "139",
        "-endy",
        "225",
        "-objects",
        "1"
        ],
        [
        "EQUAKE 1",
        "./_temp",
        "/home/caeslab/general-items/PIN/CFP2000/183.equake/exe/equake_base.none",
        "<",
        "/home/caeslab/general-items/PIN/CFP2000/183.equake/data/test/input/inp.in"
        ]
        #[ 
        # "AMMP 1",
        # "/home/caeslab/general-items/PIN/CFP2000/188.ammp/data/test/input",
        # "/home/caeslab/general-items/PIN/CFP2000/188.ammp/exe/ammp_base.none",
        # "/home/caeslab/general-items/PIN/CFP2000/188.ammp/data/test/input/ammp.in"
        #]               
        ); 


#run testcases
$count = 0;
$total = (0+@cache_configs) * (0+@testcases);
for my $config (@cache_configs) {
    for($i = 0; $i <  scalar(@testcases); $i=$i+1) {
	$count = $count + 1;
	print "\n================================================================================\nPROGRESS $count of $total \n\n"; 

	#build test command
	$iplus = $i + 1;
	$testname = $testcases[$i][0];
	$numtests = scalar(@testcases);
	print "\n\nTest $iplus of $numtests: $testname\n\n"; 
	$execfile = "";
	$outfile = $resultdir;

	#set the executable
	$execdir = $testcases[$i][1];
	$execfile = $testcases[$i][2];
	@splits = split(/\//, $testcases[$i][2]);
	$outfile = $resultdir .  @splits[-1];

	#pick up the args
	for $j (3 .. $#{$testcases[$i]}) {
	    $execfile = $execfile . " " . $testcases[$i][$j]; 
	    # for the outfile, we want just the file name,not the path
	    @splits = split(/\//, $testcases[$i][$j]);
	    $outfile = $outfile ."___". @splits[-1];
	}

	$outfile = $outfile . sprintf("-%03d", $count) . ".out";   

	#clean < and >
	$outfile =~ s/</_/g;  
	$outfile =~ s/>/_/g;  

	# Now execute.
	$tbegin = time();
	print "Executing: \n\ncd $execdir;/home/caeslab/general-items/PIN/pin-2.14/pin -t $pintool $system_config $config -o $outfile -- $execfile\n\n";
	$execout = `cd $execdir;/home/caeslab/general-items/PIN/pin-2.14/pin -t $pintool $system_config $config -o "$outfile" -- $execfile`;
	print "Executable output: \n\n$execout\n\n";

	$ttotal = time() - $tbegin;
	print "\n\nTest took approximately: $ttotal seconds\n\n"; 
    }
}

#kill any temporary files
print "\n\nCleaning up temporary files\n\n";
@tempfiles = `ls $tempdir`;
for $filename (@tempfiles) {
    chomp($filename);
    print "Unlinking temporary file: ${tempdir}${filename}\n";
    unlink("${tempdir}${filename}");
}

rmdir($tempdir);
