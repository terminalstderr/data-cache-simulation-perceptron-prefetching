// Ryan Leonard 2016 <ryan.leonard71@gmail.com>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <string>
#include <bitset>
#include "pin.H"



/********************************************************************************
 * PIN Knobs (i.e. command line arguments)
********************************************************************************/

// This knob will set the outfile name
KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool",
			    "o", "results.out", "specify optional output file name");


/********************************************************************************
 * Global Helpers and Variabls
********************************************************************************/
UINT64 aligned = 0;
UINT64 unaligned = 0;
UINT64 multiword = 0;

/********************************************************************************
 * Analysys routines
********************************************************************************/

void memAccess(UINT32 addr, UINT32 size)
{
    if (addr % size == 0)
	aligned++;
    else {
	unaligned++;
    }
    if (size > 4) {
	multiword++;
    }

}


/********************************************************************************
 * PIN Instrumentation function
********************************************************************************/

// Pin calls this function every time a new instruction is encountered
VOID Instruction(INS ins, VOID *v)
{
    if(INS_IsMemoryRead(ins))
        INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)memAccess, IARG_MEMORYREAD_EA, IARG_MEMORYREAD_SIZE, IARG_END);
    if(INS_IsMemoryWrite(ins))
        INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)memAccess, IARG_MEMORYWRITE_EA, IARG_MEMORYWRITE_SIZE, IARG_END);
}

/********************************************************************************
 * PIN Fini (tear down mechanism)
********************************************************************************/

// This function is called when the application exits
VOID Fini(INT32 code, VOID *v)
{
    // Prepare the file
    ofstream file;
    file.open(KnobOutputFile.Value().c_str());
    file.setf(ios::showbase);
    // Write Results
    file << "aligned:   " << aligned << endl;
    file << "unaligned: " << unaligned << endl;
    file << "multiword: " << multiword << endl;
    // File cleanup
    file.close();
}



/********************************************************************************
 * Pintool main function
********************************************************************************/

// argc, argv are the entire command line, including pin -t <toolname> -- ...
int main(int argc, char * argv[])
{
    // Initialize pin
    PIN_Init(argc, argv);

    // Register Instruction to be called to instrument instructions
    INS_AddInstrumentFunction(Instruction, 0);

    // Register Fini to be called when the application exits
    PIN_AddFiniFunction(Fini, 0);

    // Start the program, never returns
    PIN_StartProgram();

    return 0;
}
