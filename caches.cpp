// Ryan Leonard 2016 <ryan.leonard71@gmail.com>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <string>
#include <bitset>
#include "pin.H"
#include <algorithm>



/********************************************************************************
 * PIN Knobs (i.e. command line arguments)
********************************************************************************/

/// This knob will set the outfile name
KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool",
		"o", "results.out", "specify optional output file name");

/// This knob will set the param logPhysicalMemSize
KNOB<UINT32> KnobLogPhysicalMemSize(KNOB_MODE_WRITEONCE, "pintool",
                "m", "16", "specify the log of physical memory size in bytes");

/// This knob will set the param logPageSize
KNOB<UINT32> KnobLogPageSize(KNOB_MODE_WRITEONCE, "pintool",
                "p", "12", "specify the log of page size in bytes");

/// This knob will set the cache param logNumSets
KNOB<UINT32> KnobLogNumSets(KNOB_MODE_WRITEONCE, "pintool",
                "r", "10", "specify the log of number of rows in the cache");

/// This knob will set the cache param logBlockSize
KNOB<UINT32> KnobLogBlockSize(KNOB_MODE_WRITEONCE, "pintool",
                "b", "5", "specify the log of block size of the cache in bytes");

/// This knob will set the cache param associativity
KNOB<UINT32> KnobAssociativity(KNOB_MODE_WRITEONCE, "pintool",
                "a", "2", "specify the associativity of the cache");

/// This knob will set the cache param associativity
KNOB<UINT32> KnobPerceptronHistory(KNOB_MODE_WRITEONCE, "pintool",
                "hist", "16", "perceptron history length");



/********************************************************************************
 * Global Helpers and Variables
********************************************************************************/

//#define DEBUG
#ifdef DEBUG
const UINT32 MESSAGE_SIZE = 1024; 
char message_buf[MESSAGE_SIZE]; 
UINT32 debug_count = 0; ///< indicates count of debug messages that have been printed so far
const UINT32 MAX_DEBUG_COUNT = 1024; ///< Maximum number of debug messages to print
#endif//DEBUG
/// print message to stdout if DEBUG is defined and less than MAX_DEBUG_COUNT has been printed thus far.
void debug(string message)
{
#ifdef DEBUG
    if (debug_count < MAX_DEBUG_COUNT) 
    {
	debug_count++;
	LOG(message);
    }
#endif//DEBUG
}

UINT32 logPageSize; ///< log2 of the page size -- e.g. 12 implies 4KB page size
UINT32 logPhysicalMemSize; ///< log2 of the physical and virtual memory size -- e.g. 32 implies 1GB memory

///Function to obtain physical address given a virtual address
/**
 * This is a simple hash that returns only as many bytes as are requested based
 * on the logPhysicalMemSize parameter.
 */
UINT32 getPhysicalPageNumber(const UINT64 &virtualPageNumber)
{
    INT32 key = (INT32) virtualPageNumber;
    key = ~key + (key << 15); // key = (key << 15) - key - 1;
    key = key ^ (key >> 12);
    key = key + (key << 2);
    key = key ^ (key >> 4);
    key = key * 2057; // key = (key + (key << 3)) + (key << 11);
    key = key ^ (key >> 16);
    return (UINT32) (key&(((UINT32)(~0))>>(32-(logPhysicalMemSize))));
}



/********************************************************************************
 * Abstract CacheModel Class
********************************************************************************/

/// Abstract class which implements LRU and many other helper functions
/**
 * LRU replacement Policy: Assumes that it is sufficient to estimate LRU based on 
 * a maximum age equal to the associativity. This replacement policy uses the least
 * number of bits to make sufficient decisions, any fewer bits and we would not have
 * enough information to determine LRU in any case. Of course using more bits would 
 * be benifitial but not nessecary.
 */
class CacheModel
{
    protected:
	// Cache configuration
        UINT32   logNumSets; ///< log2 of the number of sets/rows -- e.g. 4 implies 16 sets/rows
        UINT32   logBlockSize; ///< log2 of the size of the blocks -- e.g. 3 implies 8 byte blocks
        UINT32   associativity; ///< associativity of this cache -- e.g. 2 implies 2-way associative
	// Cache statistics
        UINT64   readReqs; ///< Count of read requests
        UINT64   writeReqs; ///< Count of write requests
        UINT64   readHits; ///< Count of read hits
        UINT64   writeHits; ///< Count of write hits
	/// Array of arrays, each internal array is a set, each set contains a cache line tag
	/**
	 * Example: 
	 * Lets assume our cache has the following parameters, 
	 * associativity = 2
	 * logNumSets = 5
	 * logBlockSize = Doesn't Matter...
	 *
	 * tag = 
	 * [ set0 [UINT32 tag, UINT32 tag]
	 *   set1 [UINT32 tag, UINT32 tag]
	 *   ...
	 *   ...
	 *   ...
	 *   set32 [UINT32 tag, UINT32 tag]]
	 */
        UINT32** tagSets;
	/// Array of arrays, each internal array is a set, each set contains a cache line valid indicator
	/**
	 * Refer to tagSets for an example
	 */
        bool**   vbSets;
	/// Array of arrays, each internal array is a set, each set contains a cache line age since last access
	/**
	 * Refer to tagSets for an example
	 */
	UINT32** ageSets;
	// Magic calculation variables
	UINT32 index_offset; ///< index offset -- e.g. when logBlockSize = 2, index offset is 2
	UINT32 tag_offset; ///< tag offset -- e.g. when logNumSets = 6 and logBlockSize = 2, tag offset is 8
	UINT32 index_mask; ///< non-offset bit mask for getting index
	UINT32 tag_mask; ///< non-offset bit mask for getting tag
	UINT32 maxAge; ///< Used by our LRU algorithm, the maximum possible age which corresponds to associativity or set size
	bool directMapped;



    public:
        //Constructor for a cache
        CacheModel(UINT32 logNumSetsParam, UINT32 logBlockSizeParam, UINT32 associativityParam)
        {
            logNumSets = logNumSetsParam;
            logBlockSize = logBlockSizeParam;
            associativity = associativityParam;
	    directMapped = (associativity == 1);
            readReqs = 0;
            writeReqs = 0;
            readHits = 0;
            writeHits = 0;
	    // associativity - 1 based on our LRU policy specifications
	    maxAge = associativity - 1;
	    // Build our initial tag & valid bit list which are as big as the overall number of sets
            tagSets = new UINT32*[1u<<logNumSets]; // 1u<<logNumSets = 2^logNumSets
            vbSets = new bool*[1u<<logNumSets];
            ageSets = new UINT32*[1u<<logNumSets];
	    // Now we will fill in each set, for each set, we will add x-way worth of 'cache-lines'
	    // E.g. for a 4-way cache (associativity = 4) each set will contain 4 tags and 4 valid bits.
            for(UINT32 set_index = 0; set_index < 1u<<logNumSets; set_index++)
            {
		// build an array of tags for each set which we do not initialize since we will always check the valid bit thus
		// it doesn't matter what garbage is in the cache line except that the tag is initialized to false
                tagSets[set_index] = new UINT32[associativity];
		// build an array of valid buts for each set which MUST BE INITIALIZED TO FALSE.
                vbSets[set_index] = new bool[associativity];
                for(UINT32 j = 0; j < associativity; j++)
                    vbSets[set_index][j] = false;
		// build an array of ages -- ASSUMPTION: it is okay to assume that these will be valid values at startup
                ageSets[set_index] = new UINT32[associativity];
                for(UINT32 j = 0; j < associativity; j++)
                    ageSets[set_index][j] = maxAge;
            }       

	    LOG("================================================================================\n");
            LOG("Cache Size: " + decstr(associativity*(1u<<(logBlockSize+logNumSets-10))) + "KB\n");
	    LOG("Associativity: " + decstr(associativity) +"-way\n");
	    LOG("Block Size: " + decstr(1u<<logBlockSize) +"B\n");
	    LOG("Main Mem Size: " + decstr(1u<<(logPhysicalMemSize-30)) + "GB\n");
	    LOG("Page Size: " + decstr(1u<<(logPageSize-10)) + "KB\n");

	    // Setup our masks
	    // block_offset = 0;
	    index_offset = logBlockSizeParam;
	    tag_offset = logBlockSizeParam + logNumSets;
	    index_mask = (1u<<logNumSets) - 1;
	    LOG("block offset bit count: " + decstr(logBlockSizeParam) + "\n");
	    LOG("index bit count: " + decstr(logNumSets) + "\n");
	    LOG("tag bit count: " + decstr(logPhysicalMemSize - (logNumSets + logBlockSizeParam)) + "\n");
	    tag_mask = (1u<<(logPhysicalMemSize - (logNumSets + logBlockSizeParam))) - 1;
	    LOG("Index bitmask: " + std::bitset<32>(index_mask).to_string() + "\n");
	    LOG("Tag bitmask: " + std::bitset<32>(tag_mask).to_string() + "\n");
        }

        /// Update cache metadata or 'state' based on a cache read
        void readReq(const UINT32 &virtualAddr)
        {
	    this->readReqs++;
	    this->readHits += cacheAccess(virtualAddr);
        }

        /// Update cache meta data or 'state' based on a cache write 
        void writeReq(const UINT32 &virtualAddr)
        {
	    this->writeReqs++;
	    this->writeHits += cacheAccess(virtualAddr);
        }

        /// Prints cache statistics to the provided ofstream
        void dumpResults(ofstream &outFile)
        {
            outFile << readReqs <<","<< writeReqs <<","<< readHits <<","<< writeHits;
        }


        /// Prints cache statistics to the provided ofstream
        void dumpConfig(ofstream &outFile)
        {
            outFile << associativity*(1u<<(logBlockSize+logNumSets-10)) << "KB,"
		<< associativity << "-way,"
		<< (1u<<logBlockSize) << "B,"
		<< (1u<<(logPhysicalMemSize-30)) << "GB," 
		<< (1u<<(logPageSize-10)) << "KB,";
        }



    protected:
	/// Helper function that performs a basic cache access.
	/**
	 * When implementing this function, ensure that all of our cache meta data or 'state' is
	 * maintained by implementation. I.e. ensure that ageSets, vbSets, and tagSets are all maintained
	 * properly when performing a cache access.
	 */
	virtual UINT8 cacheAccess(const UINT32 &virtualAddr) = 0;

	/// Translates a virtual address into a physical address
	/**
	 * Prerequisite: the logPageSize global variable must be set, and getPhysicalPageNumber() function must be defined.
	 *
	 * This is function plays the role of the Memory Management unit, which takes virtual address provided
	 * by a CPU via a load/store instruction and translates it to a physical address which can then be used
	 * to access main memory or I/O.
	 *
	 * In a real system, this is performed by doing a page lookup on the Most Significant Bits (MSBs) of the 
	 * virtual address, which indicate the Virtual Page Number (VPN) which based on a page table entry is translated
	 * to a Physical Page Number (PPN). To build the Physical Address we append the PPN to the unmodified Least
	 * Significant Bits (LSBs) of our virtual address.
	 *
	 * In our simulation, the page lookup step is emulated using the getPhysicalPageNumber function.
	 *
	 * NOTICE: In this simulation, since we (1) assume that all virtual address have a unique physical address 
	 * mapping and (2) assume to not handle the case of multiple processes, then this simulation hardly effected
	 * by this emulated address translation.
	 *
	 * \param virtualAddr Virtual address to be 'looked up'
	 * \return Physical address
	 */
	UINT32 addressTranslate(const UINT32 &virtualAddr)
	{
	    UINT32 ret = virtualAddr >> logPageSize;
	    ret = getPhysicalPageNumber(ret);
	    ret = ret << logPageSize;
	    return ret | (virtualAddr & ((1u<<logPageSize) - 1));
	}

	/// Perform cache lookup given an index and a tag
	/**
	 * We want to see if a particular address exists in our cache. We use the index, tag combination
	 * to determine whether or not the lookup is successful. We iterate through the set indicated by 
	 * the index, and check if any of the cache lines contain the matching tag.
	 *
	 * \param index cache index indicating which set might contain the tag
	 * \param tag address tag that we are looking up
	 * \return true if the lookup succeeded (i.e. cache hit), false otherwise (i.e. cache miss)
	 */
	bool lookup(const UINT32 &index, const UINT32 &tag)
	{
	    UINT32 unused;
	    return lookup(index, tag, unused);
	}

	/// Lookup a value, cache line index of the set is indicated be retLineIndex iff return is true.
	bool lookup(const UINT32 &index, const UINT32 &tag, UINT32 &retLineIndex)
	{
	    bool ret = false;
	    for(UINT32 i = 0; i < associativity; i++)
	    {
		if (vbSets[index][i])
		{
		    if (tagSets[index][i] == tag) 
		    {
			ageReset(index, i);
			retLineIndex = i;
			ret = true;
		    }
		    else
		    {
			ageIncrement(index, i);
		    }
		}
	    }
	    return ret;
	}


	/// Get the index of an address (regardless of Virtual or Physical)
	/**
	 * We can take either virtual or physical addresses since our virtual and physical addresses are assumed to 
	 * be the same size in this simulation.
	 *
	 * \param address the virtual or physical address to be translated
	 * \returns the index as a UINT32, i.e. if we have 256 rows, this function will return an index between 0 and 255
	 */
	UINT32 getIndex(const UINT32 &address) 
	{
	    return (address >> index_offset) & index_mask;
	}

	/// Get the index of an address (regardless of Virtual or Physical)
	/**
	 * We can take either virtual or physical addresses since our virtual and physical addresses are assumed to 
	 * be the same size in this simulation.
	 *
	 * \param address the virtual or physical address to be translated
	 * \returns the tag as a UINT32 -- notice that the least significant bits (LSBs) are _not_ 0 padded but rather the tag is 
	 * completely contained in the lower bits of the tag. Example, if our tag is 16bits, expect the returned
	 * value to be of the form 00000000 00000000 XXXXXXXX XXXXXXXX, where all upper bits will be 0 and the LSBs will
	 * contain the tag value.
	 */
	UINT32 getTag(const UINT32 &address) 
	{
	    return (address >> tag_offset) & tag_mask;
	}

	/// Purge the cache, setting all valid bits to false
	void purge() 
	{
            for(UINT32 set_index = 0; set_index < 1u<<logNumSets; set_index++)
            {
                for(UINT32 i = 0; i < associativity; i++)
		{
                    vbSets[set_index][i] = false;
		}
	    }
	}

	/// Perform cache replacement on cache set indicated by 'index' -- should only be performed if lookup failed i.e. 'Cache Miss'
	/**
	 * \param index cache index indicating which set will contain the tag once replacement is complete
	 * \param tag address tag that we are inserting into the set
	 */
	void lruReplacement(const UINT32 &index, const UINT32 &tag) 
	{
	    // find the 'oldest' access and replace it -- notice that invalid implies infinitely old
	    UINT32 oldestIndex = 0;
	    if (!directMapped) { // optimization for direct mapped case -- oldest will be 0 if directMapped
		oldestIndex = getLeastRecentlyUsed(index);
	    }
	    replace(index, oldestIndex, tag);
	}

	/// Get the least recently used cache line out of a cache set.
	UINT32 getLeastRecentlyUsed(const UINT32 &index) 
	{
	    // find the 'oldest' access and replace it -- notice that invalid implies infinitely old
	    UINT32 oldestIndex = 0;
	    UINT32 oldest = 0;
	    // find the oldest index
	    for (UINT32 i=0; i < associativity; i++) 
	    {
		// invalid implies infinitely old, so we return as soon as we find
		// an invalid cache line
		if (vbSets[index][i] == false)
		{
		    return i;
		}
		if (ageSets[index][i] > oldest) 
		{
		    oldest = ageSets[index][i];
		    oldestIndex = i;
		}
	    }
	    return oldestIndex;
	}

	/// Perform cache line replacement 
	void replace(const UINT32 &index, const UINT32 &lineIndex, const UINT32 &tag) 
	{
	    // Perform cache line replacement 
	    vbSets[index][lineIndex] = true;
	    tagSets[index][lineIndex] = tag;
	    ageReset(index, lineIndex);
	}


	/// Increment the age of a cache line indicated by 'lineIndex' in the cache set indicated by 'index'.
	void ageIncrement(const UINT32 &index, const UINT32 &lineIndex) 
	{
	    ageSets[index][lineIndex] = min(ageSets[index][lineIndex]+1, maxAge);
	}

	/// Reset the age of a cache line indicated by 'lineIndex' in the cache set indicated by 'index'.
	void ageReset(const UINT32 &index, const UINT32 &lineIndex) 
	{
	    ageSets[index][lineIndex] = 0;
	}

	/// Get the next block after the current block
	void getNextBlock(const UINT32 &index, const UINT32 &tag, UINT32 &retindex, UINT32 &rettag)
	{
	    retindex = (index + 1) % (1u<<logNumSets);
	    if (retindex == 0)
		rettag = tag + 1;
	    else 
		rettag = tag;
	}

    private:
	/// Direct Mapped replacement should be used instead of LRU if associativity is 1
	/**
	 * This is a helper for the 'lruReplacement' to optimize for a special scenario of associativity == 1.
	 *
	 * Inside of the lruReplacement method, if the associativity of our cache indicates that we do not 
	 * need to perform the LRU calculation, i.e. (associativity == 1) == true, then we can run this replacement
	 * function and immediately return from the lruReplacement function, completely bypassing any LRU calculations.
	 */
	void dmReplacement(const UINT32 &index, const UINT32 &tag) 
	{
	    vbSets[index][0] = true;
	    tagSets[index][0] = tag;
	}
};

// These must be defined globally since we reference them in our analysis functions -- they will be instantiated in our Main function.
CacheModel* cachePP;
CacheModel* cacheOBL;
CacheModel* cacheOBLT;
CacheModel* cacheOBLT2;
CacheModel* cacheOBLT4;



/********************************************************************************
 * CacheModel Implementations
********************************************************************************/

/**
 * Perform page translation before beginning any caches accessing
 */
class LruPhysicalCacheModel: public CacheModel
{
    public:
        LruPhysicalCacheModel(UINT32 logNumSetsParam, UINT32 logBlockSizeParam, UINT32 associativityParam)
            : CacheModel(logNumSetsParam, logBlockSizeParam, associativityParam)
        {
        }

	UINT8 cacheAccess(const UINT32 &virtualAddr) 
	{
	    // Page Translation: Get physical address
	    UINT32 physicalAddr = addressTranslate(virtualAddr);

	    // Index Lookup: Lookup the index to get the appropriate set and its valid bits
	    UINT32 index = getIndex(physicalAddr);

	    // Tag Compare: 
	    UINT32 tag = getTag(physicalAddr);
	    if (lookup(index, tag)) 
		return 1;
		
	    // Upon cache miss, now we have to do some cache replacement
	    lruReplacement(index, tag);
	    return 0;
	}

};

class LruVirIndexPhysTagCacheModel: public CacheModel
{
    private:
	bool sufficientPageSize;

    public:
        LruVirIndexPhysTagCacheModel(UINT32 logNumSetsParam, UINT32 logBlockSizeParam, UINT32 associativityParam)
            : CacheModel(logNumSetsParam, logBlockSizeParam, associativityParam)
        {
	    //assert(logNumSets + logBlockSize <= logPageSize);
	    sufficientPageSize = (logNumSets + logBlockSize <= logPageSize);
        }

	UINT8 cacheAccess(const UINT32 &virtualAddr) 
	{
	    // Index Lookup: Lookup the index to get the appropriate set and its valid bits
	    UINT32 index = getIndex(virtualAddr);

	    // Page Translation: Get physical address
	    UINT32 physicalAddr = addressTranslate(virtualAddr);

	    // Tag Compare: 
	    UINT32 tag = getTag(physicalAddr);
	    if (lookup(index, tag)) // HIT! :D
		return 1;
		
	    // Upon cache miss, now we have to do some cache replacement
	    lruReplacement(index, tag);
	    return 0;
	}

};

class LruVirtualCacheModel: public CacheModel
{
    private:
	int currentPid;

    public:
        LruVirtualCacheModel(UINT32 logNumSetsParam, UINT32 logBlockSizeParam, UINT32 associativityParam)
            : CacheModel(logNumSetsParam, logBlockSizeParam, associativityParam)
        {
        }

	UINT8 cacheAccess(const UINT32 &virtualAddr) 
        {
	    // Index Lookup: Lookup the index to get the appropriate set and its valid bits
	    UINT32 index = getIndex(virtualAddr);

	    // Tag Compare: 
	    UINT32 tag = getTag(virtualAddr);
	    if (lookup(index, tag)) // HIT! :D
		return 1;
		
	    // Upon cache miss, now we have to do some cache replacement
	    lruReplacement(index, tag);
	    return 0;
        }

};

/// Prefetch on miss -- most basic version of OBL
class LruPhysicalOblTaggedCacheModel: public CacheModel
{
    private:
	bool **prefetchedSets;
	UINT32 kPrefetch; ///< Number of blocks to prefetch when performing prefetch operations

    public:
        LruPhysicalOblTaggedCacheModel(UINT32 logNumSetsParam, UINT32 logBlockSizeParam, UINT32 associativityParam, UINT32 kPrefetchParam=1)
	    : CacheModel(logNumSetsParam, logBlockSizeParam, associativityParam),
	      kPrefetch(kPrefetchParam)
        {
            prefetchedSets = new bool*[1u<<logNumSets];
            for(UINT32 set_index = 0; set_index < 1u<<logNumSets; set_index++)
            {
		prefetchedSets[set_index] = new bool[associativity];
                for(UINT32 j = 0; j < associativity; j++)
                    prefetchedSets[set_index][j] = false;
	    }
        }

    protected:
	UINT8 cacheAccess(const UINT32 &virtualAddr)
	{
	    // Page Translation: Get physical address
	    UINT32 physicalAddr = addressTranslate(virtualAddr);

	    // Index Lookup: Lookup the index to get the appropriate set and its valid bits
	    UINT32 index = getIndex(physicalAddr);

	    // Tag Compare: 
	    UINT32 tag = getTag(physicalAddr);

	    UINT32 lineIndex;
	    if (lookup(index, tag, lineIndex)) 
	    {
		// If it was a prefetch hit then we will prefetch then next k blocks as well
		if (prefetchedSets[index][lineIndex])
		{
		    prefetchKAddresses(index, tag, kPrefetch);
		}
		// Regardless of prefetching we will still return 1 if cache hit
		return 1;
	    }
		
	    // Upon cache miss, now we have to do some cache replacement and we prefetch next k blocks as well
	    lruReplacementPrefetch(index, tag, false);
	    prefetchKAddresses(index, tag, kPrefetch);
	    return 0;
	}

	/// prefetch the next 'k' addresses into our cache
	void prefetchKAddresses(const UINT32 &index, const UINT32 &tag, const UINT32 &k) 
	{
	    UINT32 nextIndex = index;
	    UINT32 nextTag = tag;
	    for (UINT32 i=0; i < k; i++) 
	    {
		getNextBlock(nextIndex, nextTag, nextIndex, nextTag);
		lruReplacementPrefetch(nextIndex, nextTag, true);
	    }
	}

	void lruReplacementPrefetch(const UINT32 &index, const UINT32 &tag, const bool &isPrefetch) 
	{
	    // find the 'oldest' access and replace it -- notice that invalid implies infinitely old
	    UINT32 oldestIndex = 0;
	    if (!directMapped) { // optimization for direct mapped case -- oldest will be 0 if directMapped
		oldestIndex = getLeastRecentlyUsed(index);
	    }
	    replacePrefetch(index, oldestIndex, tag, isPrefetch);
	}

	void replacePrefetch(const UINT32 &index, const UINT32 &lineIndex, const UINT32 &tag, const bool &isPrefetch) 
	{
	    // Perform cache line replacement 
	    replace(index, lineIndex, tag);
	    prefetchedSets[index][lineIndex] = isPrefetch;
	}

};

///
/**
 * TODO:
 * - Add a new 'super stride history' which records when (index - previousIndex) > 1
 * - Make this code subclassable
 * - Add a parameter for 
 */
class LruPhysOblTaggedPerceptronCacheModel: public LruPhysicalOblTaggedCacheModel
{
    private:
	const UINT64 MAX_HIST;
	UINT64 time;
	UINT32 previousIndex;
	// Inputs to our perceptron
	INT8 *hitsHist;
	INT8 *stridesHist;
	// Perceptron Weights and Bias (this is the model)
	INT8 *stridesWeights;
	INT8 *hitsWeights;
	INT8 perceptronBias;
	UINT64 falsePositive;
	UINT64 truePositive;
	UINT64 trueNegative;
	UINT64 falseNegative;
	INT8 stats_actual;
	INT8 stats_predict;

    public:
	LruPhysOblTaggedPerceptronCacheModel(UINT32 logNumSetsParam, UINT32 logBlockSizeParam, UINT32 associativityParam, UINT32 kPrefetchParam=1, UINT32 historyBufferSizeParam=8)
	    : LruPhysicalOblTaggedCacheModel(logNumSetsParam, logBlockSizeParam, associativityParam, kPrefetchParam),
	    MAX_HIST(historyBufferSizeParam)
        {
	    falsePositive = 0;
	    truePositive = 0;
	    trueNegative = 0;
	    falseNegative = 0;
	    time = 0;
	    previousIndex = 0;
	    hitsHist = new INT8[MAX_HIST];
	    stridesHist= new INT8[MAX_HIST];
	    // Perceptron Weights and Bias (this is the model)
	    stridesWeights= new INT8[MAX_HIST];
	    hitsWeights= new INT8[MAX_HIST];
	    for(UINT8 i = 0; i < MAX_HIST; i++)
	    {
		hitsHist[i] = 0;
		stridesHist[i] = 0;
		// We assign arbitrary default weight values
		hitsWeights[i] = ((i & 1u) ? 1 : -1);
		stridesWeights[i] = ((i & 1u) ? 1 : -1);
	    }
        }

        void dumpConfig(ofstream &outFile)
        {
            outFile << associativity*(1u<<(logBlockSize+logNumSets-10)) << "KB,"
		<< associativity << "-way,"
		<< (1u<<logBlockSize) << "B,"
		<< (1u<<(logPhysicalMemSize-30)) << "GB," 
		<< (1u<<(logPageSize-10)) << "KB,"
		<< MAX_HIST << "-slot," ;
        }

        void dumpStats(ofstream &outFile)
        {
            outFile << ", , , , ," << falsePositive <<","<< falseNegative <<","<< truePositive <<","<< trueNegative << endl;
        }

    protected:
	UINT8 cacheAccess(const UINT32 &virtualAddr) 
	{
	    UINT8 hit = 0;

	    // Page Translation: Get physical address
	    UINT32 physicalAddr = addressTranslate(virtualAddr);

	    // Index Lookup: Lookup the index to get the appropriate set and its valid bits
	    UINT32 index = getIndex(physicalAddr);

	    // Tag Compare: 
	    UINT32 tag = getTag(physicalAddr);
	    if (lookup(index, tag)) 
		hit = 1;
		
	    // Upon cache miss, now we have to do some cache replacement
	    if (hit == 0)
		lruReplacement(index, tag);

	    // We can print the prediction now and then print the 'actual' value during our update step,
	    // this will 
	    //snprintf(message_buf, MESSAGE_SIZE, "Prediction(% 2d) ", stats_predict);
	    //debug(message_buf);

	    // BEGIN PREFETCHING PERCEPTRON
	    time = (time + 1) % MAX_HIST;
	    //1. Update our perceptron weights and threshold based on the current instruction
	    updateModel(index);
	    // add the stride to our stride window
	    updateStrideHistory(index);
	    // add the hit/miss result to our hit window
	    updateHitHistory(hit);
	    // Make our prediction about the next instruction
	    INT8 prediction = predictPrefetch();
	    stats_predict = prediction;
	    // Set K accordingly
	    //kPrediction += prediction;
	    // Perform prefetching
	    prefetchKAddresses(index, tag, prediction);

	    // Gather Statistics about our cache -- stats_predict is setup just after making our prediction 
	    // call, stats_actual is determined when performing weight updates in updateModel();
	    truePositive    += ((stats_predict == 1 && stats_actual == 1) ? 1 : 0);
	    falsePositive   += ((stats_predict == 1 && stats_actual == -1) ? 1 : 0);
	    trueNegative    += ((stats_predict == 0 && stats_actual == -1) ? 1 : 0);
	    falseNegative   += ((stats_predict == 0 && stats_actual == 1) ? 1 : 0);

	    return hit;
	}

    // Needs strideHistory, hitHistory, weights,
    void updateModel(UINT32 index)
    {
	// TODO Optimization would be to move this caclulateActivation into a variable to only compute during the prediction function
	INT32 activation = calculateActivation();
	INT8 actual = (((index - previousIndex) == 1) ? 1 : -1);
	stats_actual = actual;
	if (actual*activation <= 0)
	{
	    // Update the bias
	    perceptronBias += actual;
	    // Update the weights of the perceptron
	    for (UINT64 i = 0; i < MAX_HIST; i++)
	    {
		stridesWeights[i] += actual*stridesHist[i];
		hitsWeights[i] += actual*hitsHist[i];
	    }

	}
	// Magical debugging block that really helps seeing each step of the Perceptron
	/*
	snprintf(message_buf, MESSAGE_SIZE, 
		"Actual(% 2d) -- Strides[% 2d,% 2d,% 2d,% 2d,% 2d,% 2d] SWeights[% 2d,% 2d,% 2d,% 2d,% 2d,% 2d] Hits[% 2d,% 2d,% 2d,% 2d,% 2d,% 2d] HWeights[% 2d,% 2d,% 2d,% 2d,% 2d,% 2d] Activation(% 3d)\n", 
		actual,
		stridesHist[(time+MAX_HIST-5)%MAX_HIST], hitsHist[(time+MAX_HIST-4)%MAX_HIST], hitsHist[(time+MAX_HIST-3)%MAX_HIST],
		stridesHist[(time+MAX_HIST-2)%MAX_HIST], hitsHist[(time+MAX_HIST-1)%MAX_HIST], hitsHist[(time+MAX_HIST-0)%MAX_HIST],
		stridesWeights[(time+MAX_HIST-5)%MAX_HIST], hitsWeights[(time+MAX_HIST-4)%MAX_HIST], hitsWeights[(time+MAX_HIST-3)%MAX_HIST],
		stridesWeights[(time+MAX_HIST-2)%MAX_HIST], hitsWeights[(time+MAX_HIST-1)%MAX_HIST], hitsWeights[(time+MAX_HIST-0)%MAX_HIST],
		hitsHist[(time+MAX_HIST-5)%MAX_HIST], hitsHist[(time+MAX_HIST-4)%MAX_HIST], hitsHist[(time+MAX_HIST-3)%MAX_HIST],
		hitsHist[(time+MAX_HIST-2)%MAX_HIST], hitsHist[(time+MAX_HIST-1)%MAX_HIST], hitsHist[(time+MAX_HIST-0)%MAX_HIST],
		hitsWeights[(time+MAX_HIST-5)%MAX_HIST], hitsWeights[(time+MAX_HIST-4)%MAX_HIST], hitsWeights[(time+MAX_HIST-3)%MAX_HIST],
		hitsWeights[(time+MAX_HIST-2)%MAX_HIST], hitsWeights[(time+MAX_HIST-1)%MAX_HIST], hitsWeights[(time+MAX_HIST-0)%MAX_HIST],
		activation);
	debug(message_buf);
	*/
    }

    /// Returns 1 if we should perform a prefetch, 0 otherwise
    INT8 predictPrefetch()
    {
	return (calculateActivation() > 0) ? 1 : 0; 
	//return (calculateActivation() > 0) ? 0 : 1; 
    }

    /// Calculate the activation based on our current input state of our system
    /**
     * Needs strideHistory, hitHistory, weights,
     */
    INT32 calculateActivation()
    {
	// inputs are the stride histories and the hit histories
	INT32 ret = 0;
	for (UINT64 i = 0; i < MAX_HIST; i++)
	{
	    ret += hitsHist[i]* hitsWeights[i];	
	    ret += stridesHist[i] * stridesWeights[i];	
	}
	return ret + perceptronBias;
    }

    void updateStrideHistory(UINT32 index)
    {
	stridesHist[time] = (((index - previousIndex) == 1)  ? 1 : -1);
	previousIndex = index;
    }

    void updateHitHistory(UINT8 hit)
    {
	hitsHist[time] = ((hit == 0) ? 1 : -1);
    }

};



/********************************************************************************
 * Analysys routines
********************************************************************************/
LruPhysOblTaggedPerceptronCacheModel* cacheOBLP;

void cacheLoad(UINT32 virtualAddr)
{
    //Here the virtual address is aligned to a word boundary
    // Words are assumed to be 4 bytes in our byte addressed memory scheme
    virtualAddr = (virtualAddr >> 2) << 2;
    //cachePP->readReq(virtualAddr);
    //cacheOBL->readReq(virtualAddr);
    //cacheOBLT->readReq(virtualAddr);
    //cacheOBLT2->readReq(virtualAddr);
    //cacheOBLT4->readReq(virtualAddr);
    cacheOBLP->readReq(virtualAddr);
}

void cacheStore(UINT32 virtualAddr)
{ //Here the virtual address is aligned to a word boundary // Words are assumed to be 4 bytes in our byte addressed memory scheme
    virtualAddr = (virtualAddr >> 2) << 2;
    //cachePP->writeReq(virtualAddr);
    //cacheOBL->writeReq(virtualAddr);
    //cacheOBLT->writeReq(virtualAddr);
    //cacheOBLT2->writeReq(virtualAddr);
    //cacheOBLT4->writeReq(virtualAddr);
    cacheOBLP->writeReq(virtualAddr);
}



/********************************************************************************
 * PIN Instrumentation function
********************************************************************************/

// Pin calls this function every time a new instruction is encountered
VOID Instruction(INS ins, VOID *v)
{
    if(INS_IsMemoryRead(ins))
        INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)cacheLoad, IARG_MEMORYREAD_EA, IARG_END);
    if(INS_IsMemoryWrite(ins))
        INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)cacheStore, IARG_MEMORYWRITE_EA, IARG_END);
}



/********************************************************************************
 * PIN Fini (tear down mechanism)
********************************************************************************/

// This function is called when the application exits
VOID Fini(INT32 code, VOID *v)
{
    // Prepare the file
    ofstream file;
    file.open(KnobOutputFile.Value().c_str());
    file.setf(ios::showbase);
    // Write Results
    //file << "Physical Cache, ";
    //cachePP->dumpConfig(file);
    //cachePP->dumpResults(file);
    //file << "OBL Physical Cache, ";
    //cacheOBL->dumpConfig(file);
    //cacheOBL->dumpResults(file);
    //file << "OBL Tagged Physical Cache, ";
    //cacheOBLT->dumpConfig(file);
    //cacheOBLT->dumpResults(file);
    //file << "OBL2 Tagged Physical Cache, ";
    //cacheOBLT2->dumpConfig(file);
    //cacheOBLT2->dumpResults(file);
    //file << "OBL4 Tagged Physical Cache, ";
    //cacheOBLT2->dumpConfig(file);
    //cacheOBLT2->dumpResults(file);
    file << "OBL Perceptron Physical Cache, ";
    cacheOBLP->dumpConfig(file);
    cacheOBLP->dumpResults(file);
    cacheOBLP->dumpStats(file);
    //// File cleanup
    file.close();
}



/********************************************************************************
 * Pintool main function
********************************************************************************/

// argc, argv are the entire command line, including pin -t <toolname> -- ...
int main(int argc, char * argv[])
{
    // Initialize pin
    PIN_Init(argc, argv);
	
    logPageSize = KnobLogPageSize.Value();
    logPhysicalMemSize = KnobLogPhysicalMemSize.Value();

    //cachePP = new LruPhysicalCacheModel(KnobLogNumSets.Value(), KnobLogBlockSize.Value(), KnobAssociativity.Value()); 
    cacheOBLP = new LruPhysOblTaggedPerceptronCacheModel(KnobLogNumSets.Value(), KnobLogBlockSize.Value(), KnobAssociativity.Value(), 1, KnobPerceptronHistory.Value()); 
    //cacheOBLT = new LruPhysicalOblTaggedCacheModel(KnobLogNumSets.Value(), KnobLogBlockSize.Value(), KnobAssociativity.Value()); 
    //cacheOBLT2 = new LruPhysicalOblTaggedCacheModel(KnobLogNumSets.Value(), KnobLogBlockSize.Value(), KnobAssociativity.Value(), 2); 
    //cacheOBLT4 = new LruPhysicalOblTaggedCacheModel(KnobLogNumSets.Value(), KnobLogBlockSize.Value(), KnobAssociativity.Value(), 4); 

    // Register Instruction to be called to instrument instructions
    INS_AddInstrumentFunction(Instruction, 0);

    // Register Fini to be called when the application exits
    PIN_AddFiniFunction(Fini, 0);

    // Start the program, never returns
    PIN_StartProgram();

    return 0;
}
