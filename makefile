##
## PIN tools
##

##############################################################
#
# Here are some things you might want to configure
#
##############################################################

TARGET_COMPILER?=gnu

##############################################################
#
# Tools sets
#
##############################################################

CACHE_ROOTS = caches
ACCESS_ROOTS = aligned
WORKINGMEM_ROOTS = working_mem
PIN_HOME  ?= /home/caeslab/general-items/PIN/pin-2.14

##############################################################
#
# build rules
#
##############################################################

cache: 
	g++ -DBIGARRAY_MULTIPLIER=1 -Wall -Werror -Wno-unknown-pragmas -fno-stack-protector -DTARGET_IA32 -DHOST_IA32 -DTARGET_LINUX  -I$(PIN_HOME)/source/include/pin -I$(PIN_HOME)/source/include/pin/gen -I$(PIN_HOME)/extras/components/include -I$(PIN_HOME)/extras/xed2-ia32/include -I$(PIN_HOME)/source/tools/InstLib -O3 -fomit-frame-pointer -fno-strict-aliasing   -m32 -c -o $(CACHE_ROOTS).o $(CACHE_ROOTS).cpp

	g++ -shared -Wl,--hash-style=sysv -Wl,-Bsymbolic -Wl,--version-script=$(PIN_HOME)/source/include/pin/pintool.ver    -m32 -o $(CACHE_ROOTS).so $(CACHE_ROOTS).o  -L$(PIN_HOME)/ia32/lib -L$(PIN_HOME)/ia32/lib-ext -L$(PIN_HOME)/ia32/runtime/glibc -L$(PIN_HOME)/extras/xed2-ia32/lib -lpin -lxed -ldwarf -lelf -ldl


aligned: 
	g++ -DBIGARRAY_MULTIPLIER=1 -Wall -Werror -Wno-unknown-pragmas -fno-stack-protector -DTARGET_IA32 -DHOST_IA32 -DTARGET_LINUX  -I$(PIN_HOME)/source/include/pin -I$(PIN_HOME)/source/include/pin/gen -I$(PIN_HOME)/extras/components/include -I$(PIN_HOME)/extras/xed2-ia32/include -I$(PIN_HOME)/source/tools/InstLib -O3 -fomit-frame-pointer -fno-strict-aliasing   -m32 -c -o $(ACCESS_ROOTS).o $(ACCESS_ROOTS).cpp

	g++ -shared -Wl,--hash-style=sysv -Wl,-Bsymbolic -Wl,--version-script=$(PIN_HOME)/source/include/pin/pintool.ver    -m32 -o $(ACCESS_ROOTS).so $(ACCESS_ROOTS).o  -L$(PIN_HOME)/ia32/lib -L$(PIN_HOME)/ia32/lib-ext -L$(PIN_HOME)/ia32/runtime/glibc -L$(PIN_HOME)/extras/xed2-ia32/lib -lpin -lxed -ldwarf -lelf -ldl


workspace: 
	g++ -DBIGARRAY_MULTIPLIER=1 -Wall -Werror -Wno-unknown-pragmas -fno-stack-protector -DTARGET_IA32 -DHOST_IA32 -DTARGET_LINUX  -I$(PIN_HOME)/source/include/pin -I$(PIN_HOME)/source/include/pin/gen -I$(PIN_HOME)/extras/components/include -I$(PIN_HOME)/extras/xed2-ia32/include -I$(PIN_HOME)/source/tools/InstLib -O3 -fomit-frame-pointer -fno-strict-aliasing   -m32 -c -o $(WORKINGMEM_ROOTS).o $(WORKINGMEM_ROOTS).cpp

	g++ -shared -Wl,--hash-style=sysv -Wl,-Bsymbolic -Wl,--version-script=$(PIN_HOME)/source/include/pin/pintool.ver    -m32 -o $(WORKINGMEM_ROOTS).so $(WORKINGMEM_ROOTS).o  -L$(PIN_HOME)/ia32/lib -L$(PIN_HOME)/ia32/lib-ext -L$(PIN_HOME)/ia32/runtime/glibc -L$(PIN_HOME)/extras/xed2-ia32/lib -lpin -lxed -ldwarf -lelf -ldl

## cleaning
clean:
	-rm -f *.o *.out *.tested *.failed *.d *.makefile.copy *.exp *.lib *.so *.log

realclean:
	-rm -rf *.o *.out *.tested *.failed *.d *.makefile.copy *.exp *.lib *results_* *.out

