#!/usr/bin/perl
use File::Spec;

# --- System Specifications
# 4GB of memory
$m = 34;
# 4KB page size
$p = 12;

# --- Cache Specifications
# 4B cache block frame size
$b = 2;
# 4 way associative
$a = 4;
# 1024 rows
$r = 10;

print "\nCache Size: " . (2**$b)*(2**$r)*$a/1024 . "KB";
print "\nAssociativity: " . $a . "-way";

print "\nLab 2 Testing Starting\n\n";

$tool = File::Spec->rel2abs( "./caches" ) ;
$tool = $tool . " -m $m  -p $p  -b $b  -a $a  -r $r";

$tbegin = time();
#invoke test script in this directory
open TB, "/home/caeslab/general-items/PIN/shared/drivetests.pl $tool 2>&1 |";
while(<TB>) {
  print $_;
}

$ttotal = time() - $tbegin;

print "\n\nLab 2 Testing Complete in $ttotal seconds\n\n";
